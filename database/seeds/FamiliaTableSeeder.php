<?php

use Illuminate\Database\Seeder;
use App\Models\Familia;

class FamiliaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Limpiamos tabla de familias
        \DB::table('familias')->delete();
        //Insertamos en la tabla famila
        Familia::create(['nombre' => 'FamA']);
        Familia::create(['nombre' => 'FamB']);
        Familia::create(['nombre' => 'FamC']);
        Familia::create(['nombre' => 'FamC']);
        Familia::create(['nombre' => 'FamD']);
    }
}
