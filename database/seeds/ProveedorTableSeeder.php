<?php

use Illuminate\Database\Seeder;
use App\Models\Proveedor;

class ProveedorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * protected $fillable = ['nombre','direccion','notas','valoracion'];
     * @return void
     */
    public function run()
    {
        //Limpiamos tabla
        \DB::table('proveedores')->delete();

        //Insertamos proveedores

        Proveedor::Create(['Nombre' => 'ProvA', 
            'direccion' => "Calle falsa 1 2 3", 
            'notas' => "Prov de ejemplo A", 
            'valoracion' => 5])->productos()->attach([1,4]);
        Proveedor::Create(['Nombre' => 'ProvB', 
            'direccion' => "Calle ejemplo", 
            'notas' => "Prov de ejemplo B", 
            'valoracion' => 7])->productos()->attach([2,5,3]);
        Proveedor::Create(['Nombre' => 'ProvC', 
            'direccion' => "Calle idea", 
            'notas' => "Prov de ejemplo C", 
            'valoracion' => 9])->productos()->attach([2,1,3]);
        Proveedor::Create(['Nombre' => 'ProvD', 
            'direccion' => "Calle toni", 
            'notas' => "Prov de ejemplo D", 
            'valoracion' => 2])->productos()->attach([3,5]);
        Proveedor::Create(['Nombre' => 'ProvE', 
            'direccion' => "Calle salva", 
            'notas' => "Prov de ejemplo E", 
            'valoracion' => 6])->productos()->attach([1,2,3,4,5]);
        Proveedor::Create(['Nombre' => 'ProvF', 
            'direccion' => "Calle hub", 
            'notas' => "Prov de ejemplo F", 
            'valoracion' => 1])->productos()->attach([4]);
        

    }
}
