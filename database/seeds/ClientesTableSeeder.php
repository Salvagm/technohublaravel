<?php

use Illuminate\Database\Seeder;
use App\Models\Persona;
use App\Models\Empresa;
use App\Models\Cliente;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('clientes')->delete();
        \DB::table('personas')->delete();
        \DB::table('empresas')->delete();

        // 'nombre','pseudonimo','direccion','telefono','movil','email','observaciones'
        $c1 = Cliente::Create([
            "nombre" => "Salva",
            "pseudonimo" => "salviken",
            "direccion" => "calle falsa 1 2 3",
            "telefono" => "234 567 912",
            "movil" => "689 234 123",
            "email" => "mi@mail.com",
            "observaciones" => "Esto es una observacion para Salva"
            // "tipos_cliente" =>"PERSONA"
            ]);
        $c2 = Cliente::Create([
            "nombre" => "Toni",
            "pseudonimo" => "Toniken",
            "direccion" => "calle caca 1 2 3",
            "telefono" => "234 567 912",
            "movil" => "689 234 123",
            "email" => "toni@mail.com",
            "observaciones" => "Esto es una observacion para toni"
            // "entidad_id" => 1,
            // "tipos_cliente" =>"EMPRESA"
            ]);
        $c3 = Cliente::Create([
            "nombre" => "Javi",
            "pseudonimo" => "Javiken",
            "direccion" => "calle larga 1 2 3",
            "telefono" => "234 567 912",
            "movil" => "689 234 123",
            "email" => "javi@mail.com",
            "observaciones" => "Esto es una observacion para javi"
            // "entidad_id" => 2,
            // "tipos_cliente" =>"PERSONA"
            ]);
        $c4 = Cliente::Create([
            "nombre" => "Alex",
            "pseudonimo" => "alexiken",
            "direccion" => "calle marica 1 2 3",
            "telefono" => "234 567 912",
            "movil" => "689 234 123",
            "email" => "mi@mail.com",
            "observaciones" => "Esto es una observacion para Alex"
            // "entidad_id" => 3,
            // "tipos_cliente" =>"PERSONA"
            ]);
        $c5 = Cliente::Create([
            "nombre" => "Jose",
            "pseudonimo" => "josiken",
            "direccion" => "calle meDrogo 1 2 3",
            "telefono" => "234 567 912",
            "movil" => "689 234 123",
            "email" => "jose@mail.com",
            "observaciones" => "Esto es una observacion pasa jose"
            // "entidad_id" => 2,
            // "tipos_cliente" =>"EMPRESA"
            ]);
        $c6 = Cliente::Create([
            "nombre" => "Macarena",
            "pseudonimo" => "maca",
            "direccion" => "calle de la esquina 1 2 3",
            "telefono" => "234 567 912",
            "movil" => "689 234 123",
            "email" => "maca@mail.com",
            "observaciones" => "Esto es una observacion para maca"
            // "entidad_id" => 3,
            // "tipos_cliente" =>"empresa"
            ]);

        //6 personas
        //'dni','apellido','apellido2'
        
        $p1 = Persona::Create([
            "dni" => "12312345J",
            "apellido" => "Gar",
            "apellido2" => "Men"
            ]);
        $p1->cliente()->associate($c1);
        $p1->save();

        $p3 = Persona::Create([
            "dni" => "12312865J",
            "apellido" => "Gal",
            "apellido2" => "Mar"
            ]);
        $p3->cliente()->associate($c2);
        $p3->save();

        $p2 = Persona::Create([
            "dni" => "12367845H",
            "apellido" => "Ari",
            "apellido2" => "Mar"
            ]);
        $p2->cliente()->associate($c3);
        $p2->save();

        $e1 = Empresa::Create([
            "cif" => "4124124H",
            "razon_social" => "Empresa de Informatica"
            ]);

        $e1->cliente()->associate($c4);
        $e1->save();

        $e2 = Empresa::Create([
            "cif" => "3266712H",
            "razon_social" => "Empresa de Calzado"
            ]);
        $e2->cliente()->associate($c5);
        $e2->save();

        $e3 = Empresa::Create([
            "cif" => "9982712H",
            "razon_social" => "Empresa de Peces"
            ]);
        $e3->Cliente()->associate($c6);
        $e3->save();


        
    }
}
