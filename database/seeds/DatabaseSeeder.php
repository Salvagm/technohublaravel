<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call('UserTableSeeder');
        $this->call('CategoriaTableSeeder');
        $this->call('FamiliaTableSeeder');
        $this->call('ProductTableSeeder');
        $this->call('TelefonoTableSeeder');
        $this->call('ProveedorTableSeeder');
        $this->call('EmailTableSeeder');
        $this->call('IvaTableSeeder');
        $this->call('ClientesTableSeeder');
        Model::reguard();
    }
}
