<?php

use Illuminate\Database\Seeder;
use App\Models\Iva;

class IvaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Limpiamos tabla
        \DB::table('IVA')->delete();
        //Insertamos en tabla
        Iva::Create(['tipos_iva' => 'NORMAL', 'porcentaje' => 1.21]);
        Iva::Create(['tipos_iva' => 'REDUCIDO', 'porcentaje' => 1.10]);
        Iva::Create(['tipos_iva' => 'ESPECIAL', 'porcentaje' => 1.35]);
        Iva::Create(['tipos_iva' => 'RECARGOEQUIVALENCIA', 'porcentaje' => 1.21]);

    }
}
