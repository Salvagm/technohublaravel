<?php

use Illuminate\Database\Seeder;
use App\Models\Categoria;

class CategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Limpiamos table
        \DB::table('categorias')->delete();
        // Insertamos valores
        Categoria::create(['nombre' => 'CatA','familia_id' => 1]);
        Categoria::create(['nombre' => 'CatB','familia_id' => 2]);
        Categoria::create(['nombre' => 'CatC', 'familia_id' => 2]);
        Categoria::create(['nombre' => 'CatD', 'familia_id' => 1]);
        Categoria::create(['nombre' => 'CatE', 'familia_id' => 3]);
    }
}
