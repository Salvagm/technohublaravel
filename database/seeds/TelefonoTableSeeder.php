<?php

use Illuminate\Database\Seeder;
use App\Models\Telefono;

class TelefonoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Limpiamos tabla
        \DB::table('telefonos')->delete();
        //Insertamos telefonos
        Telefono::Create(['telefono' => '695825148', 'entidad_id' => 1]);
        Telefono::Create(['telefono' => '625489971', 'entidad_id' => 2]);
        Telefono::Create(['telefono' => '656325841', 'entidad_id' => 3]);
        Telefono::Create(['telefono' => '662589753', 'entidad_id' => 4]);
        Telefono::Create(['telefono' => '658522357', 'entidad_id' => 5]);
        Telefono::Create(['telefono' => '698552354', 'entidad_id' => 6]);
        Telefono::Create(['telefono' => '654872351', 'entidad_id' => 3]);
        Telefono::Create(['telefono' => '665214879', 'entidad_id' => 4]);
        Telefono::Create(['telefono' => '614523687', 'entidad_id' => 1]);

    }
}
