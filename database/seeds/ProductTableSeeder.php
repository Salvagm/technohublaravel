<?php

use Illuminate\Database\Seeder;
use App\Models\Producto;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *  protected $fillable = ['ref', 'nombre', 'descripcion', 'precioProveedor', 'pvp', 'stockMin', 'stock'];
     * @return void
     */
    public function run()
    {
        //Limpiamos tabla
        \DB::table('productos')->delete();
        \DB::table('categoria_productos')->delete();
        \DB::table('proveedor_productos')->delete();
        //Insertamos nuevos productos
        Producto::Create(['ref' => '123456789', 'nombre' => 'ProdA', 'descripcion' => "Producto A con categorias A y B",
        					'precioProveedor' => 3, 'pvp' => 3.5, 'stockMin' => 2, 'stock' => 5])->categorias()->attach([1,2]);
        Producto::Create(['ref' => '678912345', 'nombre' => 'ProdB', 'descripcion' => "Producto B con categorias C y B",
        					'precioProveedor' => 2, 'pvp' => 2.5, 'stockMin' => 4, 'stock' => 8])->categorias()->attach([3,2]);
        Producto::Create(['ref' => '891234567', 'nombre' => 'ProdC', 'descripcion' => "Producto C con categorias A y D",
        					'precioProveedor' => 1, 'pvp' => 1.5, 'stockMin' => 3, 'stock' => 6])->categorias()->attach([1,4]);
        Producto::Create(['ref' => '234891567', 'nombre' => 'ProdD', 'descripcion' => "Producto D con categorias E y C",
        					'precioProveedor' => 7, 'pvp' => 7.5, 'stockMin' => 6, 'stock' => 10])->categorias()->attach([5,3]);
        Producto::Create(['ref' => '915234867', 'nombre' => 'ProdE', 'descripcion' => "Producto E con categorias A",
        					'precioProveedor' => 9, 'pvp' => 9.5, 'stockMin' => 1, 'stock' => 2])->categorias()->attach([1]);
    }
}
