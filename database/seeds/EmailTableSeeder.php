<?php

use Illuminate\Database\Seeder;
use App\Models\Email;

class EmailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Limpiamos tabla
        \DB::table('emails')->delete();
        //Insertamos en emails
        Email::Create(['email' => "ejemplo1@email.com", 'entidad_id' => 1]);
        Email::Create(['email' => "ejemplo2@email.com", 'entidad_id' => 2]);
        Email::Create(['email' => "ejemplo2@email.com", 'entidad_id' => 3]);
        Email::Create(['email' => "ejemplo4@email.com", 'entidad_id' => 4]);
        Email::Create(['email' => "ejemplo5@email.com", 'entidad_id' => 2]);
        Email::Create(['email' => "ejemplo6@email.com", 'entidad_id' => 5]);
        Email::Create(['email' => "ejemplo7@email.com", 'entidad_id' => 6]);
        Email::Create(['email' => "ejemplo8@email.com", 'entidad_id' => 4]);
    }
}

