<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('pseudonimo');
            $table->string('direccion');
            $table->string('telefono',32);
            $table->string('movil',32);
            $table->string('email',60);
            $table->text('observaciones');
            // $table->enum('tipos_cliente',['PERSONA','EMPRESA']);
            $table->timestamps();
            // $table->unique(['id','tipos_cliente']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes');
    }
}
