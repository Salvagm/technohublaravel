<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIvaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('IVA', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('tipos_iva',['NORMAL','REDUCIDO','ESPECIAL','RECARGOEQUIVALENCIA']);
            $table->float('porcentaje');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('IVA');
    }
}
