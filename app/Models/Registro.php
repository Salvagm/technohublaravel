<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    protected $table = 'registros';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['fecha','coste','cantidad','producto_id'];

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = ['id','updated_at','created_at'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['updated_at','created_at'];

    protected $casts = [
    	'cantidad' => 'integer',
    	'coste' => 'float'
    ];

    /**
    * Relacion inversa con familia, N categorias - 1 familia
    */
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto');
    }

}
