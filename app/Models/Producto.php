<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'productos';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['ref', 'nombre', 'descripcion', 'precioProveedor', 'pvp', 'stockMin', 'stock'];

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = ['id','updated_at'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['updated_at'];

    /**
    * Relacion muchos a muchos de productos a categoria;
    */
    public function categorias()
    {
        return $this->belongsToMany('App\Models\Categoria','categoria_productos');
    }

    public function proveedores()
    {
        return $this->belongsToMany('App\Models\Proveedor', 'proveedor_productos');   
    }

    public function registros()
    {
        return $this->hasMany('App\Models\Registro');
    }

    protected $casts = 
    [
        'stock'             => 'integer',
        'stockMin'          => 'integer',
        'pvp'               => 'float',
        'precioProveedor'   => 'float'
    ];
}
