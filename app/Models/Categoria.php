<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Familia;
use App\Models\Producto;

class Categoria extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'categorias';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['nombre', 'familia_id'];

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = ['id','updated_at', 'created_at'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['updated_at', 'created_at'];

    /**
    * Relacion inversa con familia, N categorias - 1 familia
    */
    public function familia()
    {
        return $this->belongsTo('App\Models\Familia');
    }

    /**
    * Relacion mucho a muchos de categoria a productos
    */
    public function productos()
    {
        return $this->belongsToMany('App\Models\Producto','categoria_productos');
    }
}
