<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'personas';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['dni','apellido','apellido2'];

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = ['id','updated_at','created_at'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['id','updated_at','created_at'];

    /**
    * Returns article or event data.
    *
    * @return \Illuminate\Database\Eloquent\Relations\MorphTo
    */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente','id','id');
    }
}
