<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Telefono extends Model
{
        /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'telefonos';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['telefono'];

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = ['id','updated_at'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['updated_at','created_at'];

    /**
    * Relacion de un telefono pertenece a un proveedor
    */
    public function proveedor()
    {
        return $this->belongsTo('App\Models\Proveedor');
    }

    /**
     * Relacion de un telefono pertenece a una persona del SAT
     */
    public function sat()
    {
    	return $this->belongsTo('App\Models\Sat');
    }

}
