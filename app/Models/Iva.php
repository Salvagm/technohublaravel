<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Iva extends Model
{
    protected $table = 'IVA';

     /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['tipo_iva', 'porcentaje'];

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = ['id','updated_at','created_at'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['updated_at','created_at'];

    /**
    * Relacion muchos a muchos de productos a categoria;
    */
   
   protected $casts =
   [
        'porcentaje' => 'float'
   ];
}

