<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
	   /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'proveedores';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['nombre','direccion','notas','valoracion'];

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = ['id','updated_at'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['updated_at'];

    /**
    * Relacion con telefonos de 1-N
    */
    public function telefonos()
    {
    	return $this->hasMany('App\Models\Telefono','entidad_id');
    }
    /**
    * Relacion con emails de 1-N
    */
    public function emails()
    {
    	return $this->hasMany('App\Models\Email','entidad_id');
    }

    public function productos()
    {
    	return $this->belongsToMany('App\Models\Producto', 'proveedor_productos');
    }

    protected $casts = 
    [
    	'valoracion' => 'integer'
    ];
}
