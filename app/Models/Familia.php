<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Categoria;

class Familia extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'familias';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['nombre'];

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = ['id','updated_at'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['updated_at'];

    /**
    * Relacion con categoria de 1-N
    */
    public function categorias()
    {
    	return $this->hasMany('App\Models\Categoria');
    }
}
