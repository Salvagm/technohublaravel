<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{	
	/**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'clientes';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['nombre','pseudonimo','direccion','telefono','movil','email','observaciones'];

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = ['id','updated_at','created_at'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['updated_at','created_at'];

    /**
    * Returns article or event data.
    *
    * @return \Illuminate\Database\Eloquent\Relations\hasOne
    */
    public function empresa()
    {
        return $this->hasOne('App\Models\Empresa','id','id');
    }

    /**
    * Returns article or event data.
    *
    * @return \Illuminate\Database\Eloquent\Relations\hasOne
    */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona','id','id');
    }
    
}
