<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Familia;
use App\Http\Requests\FamilyRequest;

class FamiliesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $families = Familia::all();
        // $familyData = array();
        // foreach ($variable as $key => $value) {
        //     $familyData[]
        // }
        $elements = count($families);
        $response = 200;
        $msg = "Success";
        if($elements == 0 )
        {
            $response = 204;
            $msg = "Empty";
        }
        return response()->json([
                "msg" => $msg,
                "families" => $families->toArray()
            ],$response);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(FamilyRequest $request)
    {
        $family = new Familia;
        $family->nombre = $request->nombre;

        $family->save();

        return response()->json([
            "msg" => "Success",
            "idFamily" => $family->id
        ],
        201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $family = Familia::find($id);
        $category = $family->categorias;
        return response()->json([
            "msg" => "Success",
            "family" => $family
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(FamilyRequest $request, $id)
    {
        $family = Familia::find($id);
        $family->nombre = $request->nombre;
        $family->save();

       return response()->json([
            "msg" => "Success",
            "idFamily" => $family->id
        ],201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // TODO: (1) Destruir categorias cuando se destruya la familia
        Familia::destroy($id);

        return response()->json([
            "msg" => "Delted",
            "id" => $id
        ],202);
    }
}
