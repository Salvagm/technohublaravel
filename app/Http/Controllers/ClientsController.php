<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ClientRequest;
use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\Persona;
use App\Models\Empresa;


class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($ctype = null)
    {
        $clients = null;
        $response = 200;
        $msg = "Success";
        $hasError = false;
        if($ctype === 'person')
            $clients  = Persona::with('cliente')->get();
        else if($ctype === 'company')
            $clients = Empresa::with('cliente')->get();
        else
            $hasError = true;


        $elements = count($clients);

        if($elements == 0 )
        {
            $response = 204;
            $msg = "Empty";
        }
        if($hasError === true)
        {
            $response = 422;
            $msg = "Type is undefined";
        }
        return Response()->json([
            "msg" => $msg,
            "clients" => $clients,
            "type" => $ctype
            ],$response);
    }


    /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(ClientRequest $request)
    {
        $client = null;
        \DB::transaction(function() use ($request,$client)
        {
            $concreteClient = null;
            if($request->clientType === 'company')
            {
                $concreteClient = Empresa::Create($request->concreteClient);
            }
            else
            {
                $concreteClient = Persona::Create($request->concreteClient);    
            }
            $client = Cliente::Create($request->client);
            $concreteClient->cliente()->associate($client);
            $concreteClient->save();
        });
        

        return response()->json([
            "msg" => "Success"
                ],201);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $client = Cliente::find($id)->load('persona','empresa');
        $type = "company";
        if($client->persona != null)
        {
            $type = "person";
        }

        return response()->json([
            "msg" => "Found",
            "client" => $client,
            "clientType" => $type
            ],200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(ClientRequest $request, $id)
    {
        $concreteClient = Persona::find($id);
        $error = "";
        if($request->clientType === 'company')
            $concreteClient = Empresa::find($id);
        $client = Cliente::find($id);
        
        \DB::transaction(function() use ($request,$concreteClient,$client)
        {
            $client->fill($request->client);
            $concreteClient->fill($request->concreteClient);
            $client->save();
            $concreteClient->save();
        });

        return response()->json([
            "msg" => "Success",
            "idClient" => $id,
        
        ],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        
        \DB::transaction(function() use ($id)
        {
            Cliente::destroy($id);
            $concreteClient = Persona::find($id);
            if($concreteClient !== null)
            {
                Persona::destroy($id);
            }   
            else
            {
                Empresa::destroy($id);
            }         

        });

        return response()->json([
            "msg" => "Success",
            "idCliente" => $id
        ]);

    }

}
