<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Proveedor;
use App\Models\Email;
use App\Models\Telefono;
use App\Http\Requests\StoreSuplierRequest;
use App\Http\Requests\SuplierRequest;

class SupliersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $supliers = Proveedor::all();
        $elements = count($supliers);
        $response = 200;
        $msg = "Success";
        if($elements == 0 )
        {
            $response = 204;
            $msg = "Empty";
        }
        return response()->json([
                "msg" => $msg,
                "supliers" => $supliers->toArray()
            ],$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(SuplierRequest $request)
    {
        $suplier = new Proveedor();

        \DB::transaction(function() use ($request,$suplier)
        {
            $suplier->nombre = $request->name;
            $suplier->valoracion = $request->valoration;
            $suplier->direccion = $request->location;
            $suplier->notas = $request->notes;

            $suplier->save();
            $suplierID = $suplier->id;

            foreach ($request->emails as $email ) {
                $mEmail = new Email();
                $mEmail->email = $email;
                $mEmail->entidad_id = $suplierID;
                $mEmail->save();
            }

            foreach ($request->phones as $phone) {
                $mPhone = new Telefono();
                $mPhone->telefono = $phone;
                $mPhone->entidad_id = $suplierID;
                $mPhone->save();
            }
        });

        return response()->json(
            [
                "msg" => "success",
                "suplier" => $suplier
            ],
            201);
    }

        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $suplier = Proveedor::find($id);
        $phones = $suplier->telefonos;
        $emails = $suplier->emails;
        $products = $suplier->productos;
        foreach ($products as $product) {
            $product->category = $product->categorias()->first();
        }
        return response()->json([
            "msg" => "Success",
            "suplier" => $suplier,
            "phones" => $phones,
            "emails" => $emails,
            "products" => $products
        ],200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(SuplierRequest $request, $id)
    {
        $suplier = Proveedor::find($id);

        \DB::transaction(function () use ($request, $suplier)
        {
            $deletedEmails = $request->deletedEmails;
            $deletedPhones = $request->deletedPhones;
            $suplierID = $suplier->id;
            Email::destroy($deletedEmails);    
            Telefono::destroy($deletedPhones);
            $updatedEmails = $request->updatedEmails;
            $updatedPhones = $request->updatedPhones;
            foreach ($updatedPhones as $updatedPhone) {
                $mPhone = Telefono::findOrFail($updatedPhone['id']);
                $mPhone->telefono = $updatedPhone['telefono']; 
                $mPhone->save();
            }
            foreach ($updatedEmails as $updatedEmail) {
                $mEmail = Email::findOrFail($updatedEmail['id']);
                $mEmail->email = $updatedEmail['email'];
                $mEmail->save();
            }
            foreach ($request->newPhones as $phone) {
                $mPhone = new Telefono();
                $mPhone->telefono = $phone['telefono'];
                $mPhone->entidad_id = $suplierID;
                $mPhone->save();
            }
            foreach ($request->newEmails as $email ) {
                $mEmail = new Email();
                $mEmail->email = $email['email'];
                $mEmail->entidad_id = $suplierID;
                $mEmail->save();
            }


            $suplier->nombre = $request->name;
            $suplier->valoracion = $request->valoration;
            $suplier->direccion = $request->location;
            $suplier->notas = $request->notes;
            $suplier->save();
        });

    return response()->json(
        [
            "msg" => "Success",
            "suplier" => $suplier

        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $suplier =  Proveedor::find($id);

        \DB::transaction(function() use ($suplier)
        {
            $emails = $suplier->emails;
            $phones = $suplier->telefonos;
            foreach ($emails as $email) {
                Email::destroy($email->id);
            }

            foreach ($phones as $phone) {
                Telefono::destroy($phone->id);
            }

            Proveedor::destroy($suplier->id);
        });

        return response()->json(
            [ 
                "suplier" => $suplier
            ],204);
    }
}
