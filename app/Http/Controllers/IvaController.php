<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Iva;

class IvaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $iva = Iva::all();
        return Response()->Json(
        [
            'msg' => 'Success',
            'iva' => $iva
        ],200);        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $msg = "Error";
        $response = 422;
        if(is_float($request->porcentaje) && $request->porcentaje > 0)
        {
            $msg = "Success";
            $response = 200;
            $iva = Iva::Find($id); 
            $iva->porcentaje = $request->porcentaje;
            $iva->save();
            return Response()->json([
                "msg" => $msg,
                "iva" => $iva
                ],$response);
        }

        return Response()->json([
            "msg" => $msg,
            "error" => "Porcentaje invalido, revisa que sea mayor que 0"
            ],$response);


    }

}
