<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Registro;
use App\Models\Producto;
use App\Http\Requests\RegistryRequest;

class RegistriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $registries = Registro::all();
        foreach ($registries as $registry) {
            $registry->producto;
        }
        $message = "Success";
        $response = 200;
        if(count($registries) == 0)
        {
            $response = 204;
            $message = "Empty";
        }


        return reponse()->json([
            "msg" => $message,
            "registries" => $registries
            ],$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(RegistryRequest $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $registry = Registro::find($id);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(RegistryRequest $request,$id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $registry = Registro::destroy($id);

        return reponse()->json([
            "msg" => "Success",
            "registry" => $registry
            ],204);
    }
}
