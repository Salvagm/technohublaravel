<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Producto;
use App\Models\Iva;
use App\Models\Categoria;
use App\Models\Registro;
use RegistriesController;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $products = Producto::with('categorias')->with(['proveedores' => function($query)
            {
              $query->orderBy('valoracion','desc');  
            }])->get();
        $elements = count($products);
        // foreach ($products as $product) {
        //     $product->categoria = $product->categorias()->first();
        //     $product->proveedor = $product->proveedores()->orderBy('valoracion','desc')->first();   
        // }

        $response = 200;
        $msg = "Success";
        if($elements == 0 )
        {
            $response = 204;
            $msg = "Empty";
        }
        return response()->json([
                "msg"       => $msg,
                "products"  => $products->toArray(),
            ],$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ProductRequest $request)
    {
        $product = new Producto();
        \DB::transaction(function() use ($request,$product)
        {
            
            $categoires = \DB::table('categorias')->whereIn('id', $request->categories)->get();
            $supliers = \DB::table('proveedores')->whereIn('id',$request->supliers)->get();
            $product->ref = $request->ref;
            $product->nombre = $request->nombre;
            $product->descripcion = $request->descripcion;
            $product->precioProveedor = $request->precioProveedor;
            $product->pvp = $request->pvp;
            $product->stockMin = $request->stockMin;
            $product->stock = $request->stock;
            
            $product->save();
            $product->categorias()->sync($request->categories);
            $product->proveedores()->sync($request->supliers);

        });
        return response()->json([
            "msg"       => "Success",
            "Product" => $product
        ],
        201);
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $product = Producto::find($id)->load('categorias','proveedores');
        $iva = Iva::all();
        return response()->json([
            "msg"           => "Success",
            "product"       => $product,
            "iva"           => $iva,
            "categoires"    => $product->categorias,
            "supliers"      => $product->proveedores

        ],200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(ProductRequest $request, $id)
    {
        $product = Producto::find($id);
        \DB::transaction(function() use ($request,$product)
        {
            // $category = Categoria::find(intval($request->idCat));
            // Unicamente hacemos esto para comprobar que todos los id que nos pasan son de categorias
            $categoires = \DB::table('categorias')->whereIn('id', $request->categories)->get();
            $supliers = \DB::table('proveedores')->whereIn('id',$request->supliers)->get();
            $product->ref = $request->ref;
            $product->nombre = $request->nombre;
            $product->descripcion = $request->descripcion;
            $product->precioProveedor = $request->precioProveedor;
            $product->pvp = $request->pvp;
            $product->stockMin = $request->stockMin;
            $product->stock = $request->stock;
            
            $product->save();
            $product->categorias()->sync($request->categories);
            $product->proveedores()->sync($request->supliers);

        });
        return response()->json([
            "msg"       => "Success",
            "idProduct" => $product->id
        ],
        200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = Producto::find($id);
        
        // Quitamos todas las relaciones con la categoria
        \DB::transaction(function() use ($product)
        {
            $product->categorias()->sync([]);
            $product->proveedores()->sync([]);
            foreach ($product->registros as $registry) {
                Registro::destroy($registry->id);
            }
            Producto::destroy($id);
        });

        return response()->json([
            "msg"   => "Deleted",
            "id"    => $id
        ],204);
    }


}
