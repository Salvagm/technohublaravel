<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Categoria;
use App\Models\Familia;
use App\Models\Producto;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Categoria::all();
        foreach ($categories as $category) {
            $category->productos;
        }
        $elements = count($categories);
        $response = 200;
        $msg = "Success";
        if($elements == 0 )
        {
            $response = 204;
            $msg = "Empty";
        }
        return response()->json([
                "msg" => $msg,
                "categories" => $categories->toArray()
            ],$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CategoryRequest $request)
    {
        $category = Categoria::create(array(
            'nombre' => $request->nombre,
            'familia_id' => $request->idFamilia
        ));
      

        return response()->json([
            "msg" => "Success",
            "idcategory" => $category->id
        ],
        201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $category = Categoria::find($id);
        $category->familia; // obtiene la familia y la auto añade al array de category
        $procuts = $category->productos;
        return response()->json([
            "msg" => "Success",
            "category" => $category,
            
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $category = Categoria::find($id);
        $families = Familia::find($request->idFamilia);
        $category->nombre = $request->nombre;
        $category->familia()->associate($families);
        $category->save();

        return response()->json([
            "msg" => "Success",
            "idcategory" => $category->id,
        ],
        200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $category = Categoria::find($id);
        $productos = $category->productos()->sync([]);
        Categoria::destroy($id);
        return response()->json([
            "msg" => "Delted",
            "id" => $id
        ],204);
    }
}
