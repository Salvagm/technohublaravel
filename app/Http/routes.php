<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::group(array('prefix'=>'/api'),function()
// {

// });
// Route::get('/',"ProductsController@index");

// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);

Route::get('products',"ProductsController@index");
Route::get('products/{id}',"ProductsController@show");
Route::post('products',"ProductsController@store");
Route::put('products/{id}',"ProductsController@update");
Route::delete('products/{id}', "ProductsController@destroy");
	
Route::get('families',"FamiliesController@index");
Route::get('families/{id}',"FamiliesController@show");
Route::post('families',"FamiliesController@store");
Route::put('families/{id}',"FamiliesController@update");
Route::delete('families/{id}', "FamiliesController@destroy");

Route::get('categories',"CategoriesController@index");
Route::get('categories/{id}',"CategoriesController@show");
Route::post('categories',"CategoriesController@store");
Route::put('categories/{id}',"CategoriesController@update");
Route::delete('categories/{id}', "CategoriesController@destroy");

Route::get('supliers',"SupliersController@index");
Route::get('supliers/{id}',"SupliersController@show");
Route::post('supliers',"SupliersController@store");
Route::put('supliers/{id}',"SupliersController@update");
Route::delete('supliers/{id}', "SupliersController@destroy");

Route::get('iva',"IvaController@index");
Route::put('iva/{id}', "IvaController@update");



Route::get('clients/{id}',"ClientsController@show"); // Concreto 
Route::get('clients/{type?}',"ClientsController@index"); //Todos segun el tipo
Route::put('clients/{id}',"ClientsController@update");
Route::post('clients',"ClientsController@store");
Route::delete('clients/{id}', "ClientsController@destroy");

// Route::post('supliers',"SupliersController@store");
// Route::put('supliers/{id}',"SupliersController@update");
// Route::delete('supliers/{id}', "SupliersController@destroy");
