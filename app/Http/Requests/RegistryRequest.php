<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegistryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         switch ($this->method()) {
            case 'GET':
            break;
            case 'PUT' :
                return [
                    'cantidad'      => 'required|integer',
                    'coste'         => 'required|numeric',
                    'product.id'    => 'required|exist:products',
                    'fecha'         => 'date|d-m-Y'
                ];
            
            case 'POST':
                return [
                    'registries'    => 'required|array|checkRegistries'
                ];
            break;
            case 'DELETE':
            break;
            default:
                return [
                    //
                ];
             break;
        } 

    
    }
}
