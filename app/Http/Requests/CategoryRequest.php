<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Categoria;

class CategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *  'nombre' => 'required',
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'idFamilia' => 'required|exists:familias,id'
        ];
    }
}
