<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Cliente;
use App\Models\Persona;
use App\Models\Empresa;

class ClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            break;
            case 'PUT' :
                return [
                    'client.id' => 'required|exists:clientes,id',
                    'client.nombre' => 'required|string',
                    'client.email' => 'required|email',
                    'clientType' => 'required',
                    'concreteClient.apellido' => 'required_if:clientType,person',
                    'concreteClient.dni' => 'required_if:clientType,person',
                    'concreteClient.cif' => 'required_if:clientType,company',
                    'concreteClient.razon_social' => 'required_if:clientType,company'
                ];
            
            case 'POST':
                return [
                    'client.nombre' => 'required|string',
                    'client.email' => 'required|email',
                    'clientType' => 'required',
                    'concreteClient.apellido' => 'required_if:clientType,person',
                    'concreteClient.dni' => 'required_if:clientType,person',
                    'concreteClient.cif' => 'required_if:clientType,company',
                    'concreteClient.razon_social' => 'required_if:clientType,company'
                ];
            break;
            case 'DELETE':
            break;
            default:
                return [
                    //
                ];
             break;
        } 
    }
}
