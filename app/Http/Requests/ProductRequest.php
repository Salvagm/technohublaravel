<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Producto;


class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            break;
            case 'PUT' :
                return [
                    'stockMin'          => 'required|integer|min:0',
                    'stock'             => 'required|integer|min:0',
                    'pvp'               => 'required|numeric|min:0',
                    'precioProveedor'   => 'required|numeric|min:0',
                    'categories'        => 'array',
                    'supliers'          => 'array'
                ];
            
            case 'POST':
                return [
                    'ref'               => 'required|unique:productos,ref',
                    'supliers'          => 'array',
                    'categories'        => 'array',
                    'stockMin'          => 'required|integer|min:0',
                    'stock'             => 'required|integer|min:0',
                    'pvp'               => 'required|numeric|min:0',
                    'precioProveedor'   => 'required|numeric|min:0'
                ];
            break;
            case 'DELETE':
            break;
            default:
                return [
                    //
                ];
             break;
        } 
    
    }
}
