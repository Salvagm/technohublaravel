<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Proveedor;


class SuplierRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            break;
            case 'PUT' :
                return [
                    'updatedEmails' => 'array|emailsCheck',
                    'updatedPhones' => 'array',
                    'newEmails'     => 'array|emailsCheck',
                    'newPhones'     => 'array',
                    'deletedEmails' => 'array',
                    'deletedPhones' => 'array',
                    'name'          => 'required|string',
                    'notes'         => 'string',
                    'location'      => 'required|string',
                    'valoration'    => 'required|numeric|min:0|max:9'

                ];
            
            case 'POST':
                return [
                    'name'          => 'required|string',
                    'notes'         => 'string',
                    'location'      => 'required|string',
                    'valoration'    => 'required|numeric|min:0|max:9',
                    'emails'        => 'required|array|emailsCheck',
                    'phones'        => 'required|array'
                ];
            break;
            case 'DELETE':
            break;
            default:
                return [
                    //
                ];
             break;
        } 


    }
}
