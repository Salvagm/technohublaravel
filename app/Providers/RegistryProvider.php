<?php

namespace App\Providers;
use Validator;
use Illuminate\Support\ServiceProvider;
use App\Models\Producto;

class RegistryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('checkRegistries', function($attribute, $value, $parameters)
        {
            try{
                
                foreach ($value as $registry) {
                    Producto::findOrFail($registry['product']->id);
                    $date = date_parse($registry['date']);
                    if(!is_int($registry['quantity']) || !is_float($registry['cost']) || !empty($fecha['errors']))
                        return false;
                }
                return true;
            }catch(ModelNotFoundException $ex)
            {
                return false;
            }
            
           
        });
        Validator::replacer('checkRegistries',function($message, $attribute, $rule, $parameters)
        {
            return "A registry is invalid";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
