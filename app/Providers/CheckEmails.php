<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;

class CheckEmails extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('emailsCheck', function($attribute, $value, $parameters)
        {
            $expr = "/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/";
            if(is_array($value))
            {
                foreach ($value as $item) {
                    
                    $emailString = $item;
                    if(is_array($item))
                    {
                        $emailString = $item['email'];
                    }
                
                    if(!preg_match($expr, $emailString))
                        return false;
                }
                return true;
            }
        });

        Validator::replacer('emailsCheck',function($message, $attribute, $rule, $parameters)
        {
            return "An email is invalid";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
